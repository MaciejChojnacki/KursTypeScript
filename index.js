// --- Definiowanie zmiennych: ---
const listElements  = document.querySelectorAll(".listElement");
const listTitles = document.querySelectorAll(".listElementContent h2");
const listDescriptions = document.querySelectorAll(".listElementContent p");

// Pętla odpowiadająca za zmianę atrybutów elementów listy.
for (let i = 0; i < listElements.length; i++) {
    listElements[i].addEventListener('mouseover', () => {
        listElements[i].style.boxShadow = "0 0 10px 5px #212121";
        listTitles[i].style.color = "#0072e1";
        listTitles[i].style.fontSize = "28px";
        listTitles[i].style.animation = "Fade 0.3s linear";
        listDescriptions[i].style.display = "none";
    });
    listElements[i].addEventListener('mouseleave', () => {
        listElements[i].style.boxShadow = "0 0 12px 6px #000000";
        listTitles[i].style.color = "#ffffff";
        listTitles[i].style.fontSize = "25px";
        listTitles[i].style.removeProperty("animation");
        listDescriptions[i].style.display = "flex";
    });
}

// --- Definiowanie zmiennych: ---
const mainTitles = document.querySelectorAll("h1");

// Pętla odpowiadająca za scroll strony, do tytułu tematów.
for (let i = 0; i < listElements.length; i++) {
    listElements[i].addEventListener('click', () => {
        mainTitles[i].scrollIntoView({behavior: "smooth"});
    });
}

// --- Definiowanie zmiennych: ---
let mainArticles = document.querySelectorAll(".pageArticle");
let articleSpans = document.querySelectorAll(".spanArticle");

// Funkcja odpowiadająca za pokazywanie tekstu podczas scrollowania strony.
function scrollTriggerArticles() {
    mainArticles = Array.from(mainArticles)
    mainArticles.forEach(el => {
        addObserver(el)
    })
    function addObserver(el, options) {
        let observer =
            new IntersectionObserver((entries, observer) => {
                entries.forEach(entry => {
                    if(entry.isIntersecting) {
                        entry.target.classList.add('pageArticleActive');
                        observer.unobserve(entry.target);
                    }})
            }, options)
        observer.observe(el)
    }}

// Funkcja odpowiadająca za pokazywanie elementów "span" podczas scrollowania strony.
function scrollTriggerSpans() {
    articleSpans = Array.from(articleSpans)
    articleSpans.forEach(el => {
        addSecondObserver(el)
    })
    function addSecondObserver(el, options) {
        let observer = new IntersectionObserver((entries, observer) => {
            entries.forEach(entry => {
                if(entry.isIntersecting) {
                    entry.target.classList.add('spanArticleActive');
                    observer.unobserve(entry.target);
                }})
        }, options)
        observer.observe(el)
    }}
scrollTriggerArticles();
scrollTriggerSpans();

// Przyciski na pozycjach "fixed"
const scrollUp = document.getElementById("scrollUp");
const buttonChange = document.getElementById("backgroundChange");
const documentation = document.getElementById("documentation");

scrollUp.addEventListener("click", () => {
    const list = document.getElementById("listOfSubjects");
    list.scrollIntoView({behavior: "smooth"});
})
buttonChange.addEventListener("click", () => {
    document.body.style.background = "linear-gradient(360deg, rgba(30,88,255,1) 0%, rgba(30,107,255,1) 50%, rgba(30,126,255,1) 100%)";
    buttonChange.innerHTML = "Wyłączono animację (Użyj F5)";
})
documentation.addEventListener("click", () => {
    window.open("https://www.typescriptlang.org/docs/handbook/intro.html", "_blank");
})